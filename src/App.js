import { Chart } from "./components/common/Chart";
import Charts from "./data/charts";

function App() {
  return (
    <div className="principal-container">
      {
        Charts.map((c, i) =>{
          return <Chart key={i} data={c} keyChart={i} />
        } )
      }      
    </div>    
  );
}

export default App;
