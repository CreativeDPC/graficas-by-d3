import React, { useEffect } from "react";
import c3 from "c3";
import ChartDonut from "./../../data/structure-chart";

export const Chart = ({data, keyChart}) => {
    const {category, total, devices, background} = data;

    const generateChart = () =>{
        let _colors = {};
        let _columns = devices.map(e => [e.name, e.percentage] );

        devices.forEach(e => {
            if(!_colors[e.name]) _colors[e.name] = e.color;            
        });

        const data1 = {
            ...ChartDonut, 
            bindto: `#chart-donut${keyChart}`,
            data: {
                ...ChartDonut.data,
                columns: _columns,
                colors: _colors
            }            
        };
        
        c3.generate(data1); 
    }

    const addInfoChart = () =>{
        setTimeout(() => {
            let element = document.getElementById(`chart-donut${keyChart}`);
            element.innerHTML += `<div class="info">                                        
                                        <span>${category}</span>
                                        <b>${total}</b>
                                        <div class="background-chart img-${background}"></div>
                                    </div>`;                                        
        }, 100);
    }
  
    useEffect(() => {
        generateChart();
        addInfoChart();       
    }, [category, total, devices, background, keyChart]);
  
    return (
    <div className="chart-container">    
        <div id={`chart-donut${keyChart}`} className="charts"/>
        <div className="region-data">
            {
                devices.map( (e, i) =>{
                    return <div key={`d-${i}`} className="data">
                                <h2 style={{color: e.color}}>{e.name}</h2>
                                <b>{e.percentage}%</b> <span>{e.amount}</span>
                            </div>
                })
            }
        </div>
    </div>)
};