const ChartDonut = {
    bindto: "",
    data: {
      columns: [],
      type: "donut",
      colors: {}
    },
    donut: {
      width: 10,
      expand: false,
      label: {
        show: false
      }
    },
    legend: {
      hide: true  
    }
  };
  export default ChartDonut;