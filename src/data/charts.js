const Charts = [
    {
        category: 'Revenue',
        total: '200.000€',
        background: 'green',
        devices:[
            {
                name: 'Tablet',
                percentage: 60,
                amount:'120.000€',
                color: '#a1bb38'
            },
            {
                name: 'Smartphone',
                percentage: 40,
                amount:'80.000€',
                color:'#4e5f0b'
            }        
        ]
    },
    {
        category: 'Impresions',
        total: '500.000.000',
        background: 'blue',
        devices:[
            {
                name: 'Tablet',
                percentage: 40,
                amount:'200.000.000',
                color: '#14bfe6'
            },
            {
                name: 'Smartphone',
                percentage: 60,
                amount:'300.000.000',
                color:'#006177'
            }        
        ]
    },
    {
        category: 'Visits',
        total: '600.000.000',
        background: 'yellow',
        devices:[
            {
                name: 'Tablet',
                percentage: 80,
                amount:'480.000.000',
                color: '#daa93c'
            },
            {
                name: 'Smartphone',
                percentage: 20,
                amount:'120.000.000',
                color:'#966100'
            }        
        ]
    }        
];

export default Charts;