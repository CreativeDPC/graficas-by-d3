
## Contácto

Daniel Pérez Cabrera    
Cel: 55 1120 0160    
Email:  creativedesing.dpc@gmail.com

## Descripción

Ejemplo de Gráficas con React Js y D3


## Instalación

```bash
$ yarn install
```

## Ejecución

```bash
# Desarrollo
$ yarn start

```

## Tecnologías Utilizadas

- [React Js](https://es.reactjs.org/)
- [C3](https://c3js.org/)
- [D3](https://d3js.org/)


## Prueba en vivo

[Gráficas](https://graficas-831ac.web.app/)

## Imágenes

![](https://bitbucket.org/CreativeDPC/graficas-by-d3/raw/3efd5e82d85256a4cb47385ea30fa85d37efab5b/screenshots/graficas.png)
